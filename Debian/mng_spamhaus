#!/bin/sh
#set -x
clear
###
#
# Author: Stéphane HUC
# mail: devs@stephane-huc.net
#
# License: BSD Simplified
#
# Github: https://git.framasoft.org/hucste/tools
#
# Date: 2016/03/01
#
###

###
#
# Edit your crontab:
# 0 1 * * * /folder/mng_spamhaus
#
# or, launch-it, manually! ;-)
#
###

export LC_ALL=C

action="DROP"  # action iptables ; default : DROP - modify at yours risks !
chain="" - modify at yours risks !
#interface="eth" # DO NOT TOUCH ! or, at yours risks...

now="$(date +"%x %X")"
hours=3600   # in seconds

spamhaus_url="http://www.spamhaus.org/drop/"

DIRNAME="$(dirname $(readlink -f -- "$0"))"

IPT=""

SPAMHAUS_DIR="$DIRNAME/spamhaus/"
SPAM_FILE_DROP="drop.txt"
SPAM_FILE_EDROP="edrop.txt"


block() {

    printf "########## Attempt to blocks Bad IPS Spamhaus ##########\n"

    if [ -n "$chain" ]; then
        $IPT -F "$chain"
        sleep 1
        $IPT -L "$chain" -nv --line-numbers
        sleep 1
    fi

    for name in "$SPAM_FILE_DROP" "$SPAM_FILE_EDROP"; do

        file="$SPAMHAUS_DIR$name"

        if [ -f "$file" ]; then

            while IFS=$'\n' read -r line; do
                info="$(echo "$line" | tr -s ' ')"

                echo "$info" | grep -q "^;"
                if [ $? -ne 0 ]; then
                    adr_ip="$(echo "$info" | awk -F ';' '{print $1}' | xargs)"
                    sbl="$(echo "$info" | awk -F ';' '{print $2}' | xargs)"
                    echo "ip: $adr_ip ; sbl: $sbl"

                    if [ -n "$chain" ]; then
                        $IPT -A "$chain" -s "$adr_ip" -j "$action" -m comment --comment "DROP Spamhaus src $sbl"
                        $IPT -A "$chain" -d "$adr_ip" -j "$action" -m comment --comment "DROP Spamhaus dst $sbl"
                    else
                        $IPT -A INPUT -i "$link" -s "$adr_ip" -j "$action" -m comment --comment "DROP Spamhaus src $sbl"
                        $IPT -A FORWARD -i "$link" -s "$adr_ip" -j "$action" -m comment --comment "DROP Spamhaus src $sbl"
                        $IPT -A FORWARD -o "$link" -d "$adr_ip" -j "$action" -m comment --comment "DROP Spamhaus dst $sbl"
                        $IPT -A OUTPUT -o "$link" -d "$adr_ip" -j "$action" -m comment --comment "DROP Spamhaus dst $sbl"
                    fi
                fi

            done < "$file"

        fi

    done

}

detect_link() {

    printf "########## Detecting devices informations ##########\n"

    while read -r info; do

        printf "iface: ${info}\n"
        link="$(echo "${info}" | awk -F '/' '{print $5}')"
        printf "link: ${link}\n"

        if [ -n "${link}" ]; then

            if [ "$(cat "$info/carrier")" -eq 1 -a "$(cat "$info/operstate")" = "up" ]; then

                printf "\t => Checking $link ...\n"
                link="$(echo "$info" | awk -F '/' '{print $5}')"
                printf "\t => Checking $link ...\n"

                show=$(ip -f inet addr show "$link" | awk '/inet/ {print $2}')
                if [ -n "$show" ]; then address=${show%/*} ; echo "ip: $address"; fi

            fi

        fi

    done <<EOF
    $(find /sys/class/net/ | grep -v "^/sys/class/net/$" | grep -v "lo")
EOF
    unset i

    sleep 1

    }

get_spamhaus_files() {

    printf "########## Attempt to get spamhaus files ##########\n"

    for name in "$SPAM_FILE_DROP" "$SPAM_FILE_EDROP"; do

        file="$SPAMHAUS_DIR$name"

        if [ -f "$file" ]; then

            # get last modifications file in seconds
            if [ -x $(which stat) ]; then
                file_seconds=$(stat -c "%Y" "$file")
            else
                file_seconds=$(date -r "$file" +%s)
            fi

            today="$(date -d "$now" +%s)"

            # calcul diff time in seconds
            if [ -x $(which bc) ]; then
                diff_sec=$(echo "$today - $file_seconds" | bc)
            else
                diff_sec=$(($today - $file_seconds))
            fi

            unset file_seconds today

            if [ $diff_sec -gt $hours ]; then

                # obtain spamhaus file
                if [ -x $(which curl) ]; then
                    curl -A "Mozilla/5.0" -o "$file" "$spamhaus_url$name"

                elif [ -x $(which wget) ]; then
                    wget --user-agent="Mozilla/5.0" -O "$file" "$spamhaus_url$name"

                fi

                if [ $? -eq 0 ]; then
                    printf "[ \\33[0;32m%s\\33[0;39m ] %s \n" "OK" "The file $name is correctly downloaded!"
                else
                    printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "It seems to have a problem with download file $name!"
                fi

            else
                printf "Please, retry in more than one hour to attemp to downloading $spamhaus_url$name \n"
            fi

            unset diff_sec

        fi

    done

    }

verify_need_dirs() {

    if [ ! -d "$SPAMHAUS_DIR" ]; then mkdir "$SPAMHAUS_DIR"; fi

    }

verify_need_soft() {

    if [ ! -x $(which iptables) ]; then
        printf "Missing iptables command line tool, exiting. \n" >&2
        exit 1
    else
        IPT="$(which iptables)"
    fi

    }

verify_need_variables() {

    if [ -z "$action" ]; then
        printf "[ \\33[1;31m%s\\33[0;39m ] %s $0\n" "ERROR" "Necessary to set variable action ! Edit the script:"
        exit 1;
    fi

    }

verify_uid() {

    if [ $(id -u) -ne 0 ]; then
        printf "[ \\33[1;31m%s\\33[0;39m ] %s \n" "KO" "Need to get rights admins!"
        exit 1
    fi

    }

verify_uid
verify_need_soft
verify_need_variables
verify_need_dirs
detect_link
get_spamhaus_files
block

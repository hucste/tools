For nginx, modify your server config file as :

    location /blackhole/blackhole.dat {
        satisfy any;
        deny all;
    }

After, this modification, restart nginx webserver!

TODO: 
chmod 0600 /blackhole/blackhole.dat - if not function, chmod 0604; worth 0644
chmod 0404 onto /blackhole/*.php files for best secure; at minimum, 0604; worth 0644

* 0604: to allow you rewrite/upload modificated files.

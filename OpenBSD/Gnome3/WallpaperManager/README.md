Wallpapers Manager
==================

[ English ]

Script shell to manage random wallpapers for background and screensaver into Gnome3

Configuration
-------------

Modify only variable *path* into script WallpapersRandomize! **(line 30)**
/!\  **Do not touch others variables!** /!\ 

Use-it
------

Without args, only run-it!

./WallpapersRandomize

with arguments: 

- delete: to delete into home 
- install: to install into home

Install file .desktop into ~/.local/share/applications
and the script ''WallpapersRandomize'' into ~/bin/

After install, think to set into 'setting tools' > 'Applications to startup'.
And choose 'WallpaperManager'

Restart your session!

[ French ]

Script pour gérer aléatoirement un fond d'écran pour l'arrière plan et l'écran de veille du bureau Gnome3

Configuration
-------------

Modifiez juste la variable *path* dans le script WallpapersRandomize ! **(ligne 30)**
/!\ **Ne touchez pas les autres variables !** /!\

Utilisation
-----------

Sans arguments, pour son fonctionnement:

./WallpapersRandomize

Avec arguments : 

- delete : pour supprimer de son répertoire personnel
- install : pour installer dans son répertoire personnel

Installe le fichier .desktop dans ~/.local/share/applications
et le script ''WallpapersRandomize'' dans ~/bin/

Après l'installation, pensez à paramétrez dans 'Outils de personnalisation' > 'Applications au démarrage'
Et, choisissez 'Gestionnaire de fonds d'écran'

Redémarrez votre session !

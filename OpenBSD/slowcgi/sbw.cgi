#!/bin/sh
#set -x
set -e
#set -u
### 
# sbw.cgi - 0.6.3 - 2020/05/25 18:20:00
#
# cgi script to serve gzipped file if available with httpd.
# 
# Author: prx <prx@ybad.name>
#   parts stolen from romanzolotarev here : 
#   https://www.romanzolotarev.com/bin/form
#
# Collaborator: PengouinBSD <dev+bsd@huc.fr.eu.org>
#	enable deflate, brotli support
#	add header Content-Length, Last-Modified, Transfer-Encoding
#	test if dependencies are into web chroot
#	add debug to trace variables; see: /var/log/messages
#	detect if Firefox >= v64, because atom and rss feed are not managed!
# 
# Licence: MIT
#
# Description :
# When a file is requested, we try to deliver the compressed file if it is present.
# i.e. index.html.gz is send instead of plain index.html, or index.html.br
# 	web client need to support accept-encoding brotli format!
# 
# Need:
#	at minimum: chmod +x
#	better: install -o www -g bin -m 0550 sbw.cgi /var/www/cgi-bin/sbw.cgi
#
# chroot dependencies:
# in /var/www/bin: cat date sh sha256
# in /var/www/usr/bin: basename logger stat
# in /var/www/usr/lib: libc.so.96.0 - need for stat
# in /var/www/usr/libexec: ld.so - need for stat
#
# CHANGELOG:
# Add ETag header support for better cache.
#	v6.7: libc.so.96.0
#	v6.6: libc.so.95.1
#
# TODO :
# 	Think to rm -fP old lib!
###

#PATH_INFO="index.html"
: "${cachecontrol:=3600}"
: "${chroot:=/var/www}"
: "${HTTP_IF_NONE_MATCH:=}"
: "${HTTP_USER_AGENT:=}"
: "${PATH_INFO?PATH_INFO environement variable must be set with the relative path from realroot}"
: "${realroot:=.}"

#logger "$0: UA: $HTTP_USER_AGENT"

check_bin() {
	for bin in cat date sh sha256; do
		test -x "/bin/${bin}" || http500 "${bin}"
	done
	
	for bin in awk basename grep logger stat; do
		test -x "/usr/bin/${bin}" || http500 "${bin}"
	done
}

check_deps() {
	check_bin
	check_lib
}

check_encoding() {
    test -z "${HTTP_ACCEPT_ENCODING}" || encode=1
    if [ "${debug}" -eq 1 ]; then logger "$0: variable HTTP_ACCEPT_ENCODING: $HTTP_ACCEPT_ENCODING"; fi
}

check_error() {
	if [ "${error}" -eq 0 ]; then error=200; fi
}

check_lib() {
	test -f /usr/lib/libc.so.* || http500 "/usr/lib/libc.so.*"
	test -f /usr/libexec/ld.so || http500 "/usr/libexec/ld.so"
}

check_none_match() {
    test -z "${HTTP_IF_NONE_MATCH}" && return
    if [ "${etag}" = "${HTTP_IF_NONE_MATCH}" ]; then error=304; fi
    if [ "${debug}" -eq 1 ]; then logger "$0: variable HTTP_IF_NONE_MATCH: $HTTP_IF_NONE_MATCH"; fi
}

check_ua() {
	if [ -n ${HTTP_USER_AGENT:+Firefox} ]; then
		local n="${HTTP_USER_AGENT##*/}"; ffv="${n%%.*}"; unset n
		if [ "${debug}" -eq 1 ]; then logger "$0: FF Version: $ffv"; fi
	fi
}

check_url() {
	if [ -d "${call}" ]; then
		call="${realroot}${PATH_INFO}index.html"
	fi
}

content_type() {
	local file="$1"
	
	case "${file}" in
		*atom.xml*|*.atom) type="application/atom+xml" ;;
		*.css)  type="text/css" ;;
		*.html) type="text/html; charset=utf-8" ;;
		*.js)   type="application/javascript" ;;
		*.json)	type="application/json" ;;
		*rss.xml*)	type="application/rss+xml" ;;
		*.svg)  type="image/svg+xml" ;;
		*.txt)  type="text/plain; charset=utf-8" ;;		 
		*.xml)  type="text/xml" ;;	 
	esac
	
	check_ua
	if [ "${ffv}" -ge 64 ]; then
		case "${file}" in
			atom.xml|rss.xml) type="text/xml" ;;
		esac
		if [ "${debug}" -eq 1 ]; then logger "$0: file: $file; type: $type"; fi
	fi
	
	if [ "${debug}" -eq 1 ]; then logger "$0: variable type: $type"; fi
	if [ "${type}" = "" ]; then
		logger "$0: Can't obtain mime-type, for this file: '${file}'!"
	fi
}

echo_status() {
	echo "Status: ${title}"
}

get_http() {
	local title=""
	mng_title
	
	case "${error}" in
        200) http200 ;;
        304) http304 ;;
        404) http404 ;;
        500) http500 ;;
        *)
			logger "$0: This error '${error}' is invalid!"
			exit 1 
        ;;
    esac
}

html() {
	local name="$1"
	local h1="<h1>$title</h1>"
	local title="<title>$title</title>"
	
	local html_header='<!DOCTYPE html>
<head>
	<meta charset="utf-8">
	<style type="text/css">
	body { background-color: black; color: white; font-family: "Comic Sans MS", "Chalkboard SE", "Comic Neue", sans-serif; }
	h1 { color: red; font-weight: bold; }
	hr { border: 0; border-bottom: 1px dashed; }
	</style>
'
	
	local html_h2b='
</head>
<body>
'
	local html_end='
	<hr>
	<address>OpenBSD httpd</address>
</body>
'

	if [ "${name}" != "" ]; then 
		local p="<p>This dependency lacks: ${name}; please, install-it into the chroot: ${chroot}</p>"
	fi
	
	local html="${html_header}\n${title}\n${html_h2b}\n${h1}\n$p\n${html_end}\n"
	
	echo "Content-Type: text/html; charset=utf-8"
	echo ""
	echo "${html}"
}

http200() {
	local type=""
	content_type "${filename}"
	
    echo_status
    if [ "${cachecontrol}" -ge 1 ]; then echo "Cache-Control: max-age=${cachecontrol}"; fi
    if [ "${encode}" -eq 1 ]; then echo "Content-Encoding: ${serve}"; fi
    if [ "${size}" -ge 1 ]; then echo "Content-Length: ${size}"; fi
    if [ "${type}" != "" ]; then echo "Content-Type: ${type}"; fi
    if [ "${etag}" != "" ]; then echo "ETag: ${etag}"; fi
    if [ "${mtime}" != "" ]; then echo "Last-Modified: ${mtime}"; fi
    if [ "${serve}" != "" ]; then echo "Transfer-Encoding: ${serve}"; fi
    echo ""

    cat "$file"
    exit 
}

http304() {
    echo_status
    echo ""
    exit
}

http404() {
    echo_status    
    html "${filename}"
    exit
}

http500() {
	local error=500
	local name="$1"
	local title=""
	
	mng_title
	
	echo_status
	
	if [ "${name}" != "" ]; then logger "$0: This bin '${name}' lacks; please intall into chroot: '${chroot}'!"; fi
	html "${name}"
	
	exit
}

main() {
	local call="${realroot}${PATH_INFO}"
	local debug=0
    local encode=0
    local error=0
    local etag=""
    local file=""
    local filename=""
    local ffv=0
	local mtime=""
    local serve=""
    local size=0
    
    if [ "${debug}" -eq 1 ]; then
		logger "$0: variable HTTP_USER_AGENT: $HTTP_USER_AGENT"; 
		logger "$0: variable PATH_INFO: $PATH_INFO"; 
		logger "$0: variable realroot: $realroot"; 
		logger "$0: variable call: $call"; 
		logger "$0: variable cachecontrol: $cachecontrol"; 
    fi
    
    check_deps
    
    check_encoding
    
    mng_file

    get_http
}

mng_file() {
	
	check_url

	if [ "${encode}" -eq 1 ]; then
        case "${HTTP_ACCEPT_ENCODING}" in
            *br*)
				file="${call}.br"
				serve="br"
			;;
            *deflate*|*gzip*)
				file="${call}.gz"
				serve="gzip"
			;;
            *)
				logger "$0: This encoding '${HTTP_ACCEPT_ENCODING}' seems invalid or not supported!"
				file="${call}"
				serve=""
            ;; 
        esac
    else
		file="${call}"
    fi
    if [ "${debug}" -eq 1 ]; then 
		logger "$0: variable encode: $encode"; 
		logger "$0: variable file: $file"; 
		logger "$0: variable serve: $serve"; 
    fi
    
	if [ -f "${file}" ]; then 
        set_etag
        
        check_none_match
        
        set_filename
        
        set_mtime
        
        set_size      
        
        check_error
    else
        error=404
    fi
    if [ "${debug}" -eq 1 ]; then logger "$0: variable error: $error"; fi
}

mng_title() {
	case "${error}" in
		200) title='200 OK' ;;
		304) title='304 Not Modified' ;;
		404) title='404 Not Found' ;;
		500) title='500 Internal Server Error' ;;
	esac
	if [ "${debug}" -eq 1 ]; then logger "$0: variable title: $title"; fi
}

set_etag() {
	etag="$(sha256 -q "${file}")"
	if [ "${debug}" -eq 1 ]; then logger "$0: variable etag: $etag"; fi
}

set_filename() {
	filename="$(basename "${call}")"
	if [ "${debug}" -eq 1 ]; then logger "$0: variable filename: $filename"; fi
}

set_mtime() {
	local timestamp
	timestamp="$(stat -f"%m" "${file}")"
	mtime="$(date -r $timestamp +"%+")"
	if [ "${debug}" -eq 1 ]; then logger "$0: variable mtime: $mtime"; fi
}

set_size() {
	size="$(stat -f"%z" "${file}")"
	if [ "${debug}" -eq 1 ]; then logger "$0: variable size: $size"; fi
}

main
